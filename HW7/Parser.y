%{
#include "Parser.h"

using namespace std;
int yyerror (const char * er);
extern int yylex ();
extern int yylineno;
extern char * yytext;
int string_count = 0;
int float_count = 0;
int cmp_count = 0;
int and_count = 0;
int or_count = 0;
int if_count = 0;
int while_count = 0;
int fp = 4;

struct variable {
    char* name;
    char type;
    int offset;
};

// I know I'm wasting space by having both a float and a char* when I need one or the other
// The type and number are combined to make the label for the data
struct data {
    float f_val;
    char* s_val;
    char type;
    int index;
};

// Anything being stored as a varriable needs to know its name, what type of varriable it is, and its position on the stack.
vector<variable> myvars;

// Anything being put on the data segment just needs to know its type and value
vector<data> mydata;

// Used to keep track of what type the union is using
// Probably a better way to do this, but this works fine for variable asignments

%}
%union {
  int an_int;
  float a_float;
  char *a_string;
  char a_char;
}

%left '+' '-'
%left '%' '*' '/'
%nonassoc UNARY
%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%token <an_int> INT
%token <a_float> FLOAT
%token <a_string> STRING
%token <a_string> WORD
%token UNARY
%token COMMENCEMENT
%token ECRIVEZ
%token PENDANT
%token IF
%token ELSE
%token <a_char> ENTIER
%token <a_char> REEL
%token EQUAL
%token NEQUAL
%token GTE
%token LTE
%token AND
%token OR

%%

PROGRAM : 
	{
		// Start the main method of the program
		cout << "\t.text\n\t.globl main\n";
	}
	FUNCTION_DECLS
	{
		cout << "\tli $v0, 10\n\tsyscall\n";
		print_data();
	}

FUNCTION_DECLS : FUNCTION_DECLS FUNCTION_DECL
	|
	;

FUNCTION_DECL : VALUE WORD '(' ')' '{' STATEMENTS '}'
	| VALUE COMMENCEMENT 
		{
			cout << "main:\n";
			// Initialize the frame pointer to the stack pointer
			cout << "\tmove $fp, $sp\n";
		}
		'(' ')' '{' STATEMENTS '}' 
	;

INNER_STATEMENTS : INNER_STATEMENTS INNER_STATEMENT
	|
	;
	
INNER_STATEMENT : FUNCTION_CALL ';'
	| VARIABLE_ASIN ';'
	| {yyerror("Internal variable declaration");} VARIABLE_DECL ';'
	| WHILE_LOOP
	| IF_STMT 
	| error '}'
	| error ';'
	;

STATEMENTS : STATEMENTS STATEMENT
	|
	;
	
STATEMENT : FUNCTION_CALL ';'
	| VARIABLE_ASIN ';'
	| VARIABLE_DECL ';'
	| WHILE_LOOP
	| IF_STMT 
	| error '}'
	| error ';'
	;

BRACE_STATEMENTS : INNER_STATEMENT 
	| '{' INNER_STATEMENTS '}'
	;

IF_STMT : IF_TEST BRACE_STATEMENTS ELSE 
		{
			cout << "_falseif" << $<an_int>1 << ":\n";
			cout << "\tbeq $t0,1,_falseelse" << $<an_int>1 << "\n";
		}
		BRACE_STATEMENTS
		{
			cout << "_falseelse" << $<an_int>1 << ":\n";
		}
	| IF_TEST BRACE_STATEMENTS 
		{	
			cout << "_falseif" << $<an_int>1 << ":\n";
		}
		%prec LOWER_THAN_ELSE
	;

IF_TEST : IF 
		{
			$<an_int>1 = if_count++;
			$<an_int>$ = $<an_int>1;
		}
		'(' SCBOOL ')'
		{
			cout << "\tbeq $t0,0,_falseif" << $<an_int>1 << "\n";
		}

WHILE_LOOP : PENDANT 
		{
			$<an_int>1 = while_count++;
			cout << "_begwhile" << $<an_int>1 << ":\n";
		}
		'(' EXPRESSION 
		{
			if ($<a_char>4 == 'f') yyerror("Cannot have break condition be a float");
			cout << "\tbeq $t0,0,_endwhile" << $<an_int>1 << "\n";
		}
		')' BRACE_STATEMENTS
		{
			cout << "\tb _begwhile" << $<an_int>1 << "\n";
			cout << "_endwhile" << $<an_int>1 << ":\n";
		}
	;

TYPE : ENTIER
	| REEL
	;

VARIABLE_DECL : TYPE WORD_LIST

VARIABLE_ASIN : WORD '=' EXPRESSION
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if ($<a_char>3 == 'i') {
				cout << "\tlw $t0, ($sp)\n";
				intstuff ($<an_int>3, myvar);
			} else if ($<a_char>3 == 'f') {
				create_push_data($<a_char>3, &($<a_float>3));
				cout << "\tl.s $f0, ($sp)\n";
				floatstuff ($<a_float>3, myvar);
			} else if ($<a_char>3 == 'w') {
				variable* twovar = search_for_var($<a_string>3);
				if (twovar == NULL) {
					yyerror("Variable not declaired.");
					return 0;
				}
				if (twovar->type == 'i') {
					cout << "\tlw $t0,-" << twovar->offset << "($fp)\n";
					intstuff (0, myvar);
				}
				if (twovar->type == 'f') {
					cout << "\tl.s $f0,-" << twovar->offset << "($fp)\n";
					floatstuff (0, myvar);
				}
			}
			cout << "\tadd $sp,$sp,4\n";
		}

FUNCTION_CALL : ECRIVEZ '(' PRINT_TYPE ')'
	| WORD '(' VALUES ')'
	;

PRINT_TYPE : STRING  
		{
			create_push_data('s', $1);
			cout << "\tli $v0,4\n";
			cout << "\tla $a0,str" << string_count << "\n";
			cout << "\tsyscall\n";
		}
	| EXPRESSION
		{
			if ($<a_char>1 == 'i') {
				cout << "\tli $v0,1\n";
				cout << "\tlw $a0,($sp)\n";
				cout << "\tsyscall\n";
			}
			if ($<a_char>1 == 'f') {
				cout << "\tli $v0,2\n";
				cout << "\tl.s $f12,($sp)\n";
				cout << "\tsyscall\n";
			}
		}
	;

WORD_LIST : WORD_LIST ',' WORD {
		push_to_myvars($<a_char>0, $3);
	}
	| WORD {
		push_to_myvars($<a_char>0, $1);
	}
	;

VALUES : VALUES ',' VALUE
	| VALUE
	;

VALUE : INT
	| FLOAT
	| STRING
	| WORD 
	;

NUMVAL : INT
		{
			$<a_char>$ = 'i';
			cout << "\tli $t0," << $1 << "\n";
			cout << "\tsw $t0,($sp)\n";
		}
	| FLOAT 
		{
			$<a_char>$ = 'f';
			create_push_data('f', &($1));
			cout << "\tl.s $f0,fl"<< float_count <<"\n";
			cout << "\ts.s $f0,($sp)\n";
		}
	| WORD 
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if (myvar->type == 'i') {
				$<a_char>$ = 'i';
				cout << "\tlw $t0,-" << myvar->offset << "($fp)\n";
				cout << "\tsw $t0,($sp)\n";
			}
			if (myvar->type == 'f') {
				$<a_char>$ = 'f';
				cout << "\tl.s $f0,-" << myvar->offset << "($fp)\n";
				cout << "\ts.s $f0,($sp)\n";
			}
		}
	;

EXPRESSION: EXPR
	| SCBOOL {$<a_char>$ = 'i';}
	;

SCBOOL: SCBOOL OR 
		{
			$<an_int>2 = or_count++;
			cout << "lw $t0,($sp)\n";
			cout << "beq $t0,1,_skipor" << $<an_int>2 << "\n";
		}
		COND
		{
			cout << "_skipor" << $<an_int>2 << ":\n";
		}
	| SCBOOL AND
		{
			$<an_int>2 = and_count++;
			cout << "lw $t0,($sp)\n";
			cout << "beq $t0,0,_skipand" << $<an_int>2 << "\n";
		}
		COND
		{
			cout << "_skipand" << $<an_int>2 << ":\n";
		}
	| '(' SCBOOL ')'
	| '!' '(' SCBOOL ')'
		{
			cout << "\tlw $t0,($sp)\n";
			cout << "\tseq $t0,$t0,0\n";
			cout << "\tsw $t0,($sp)\n";
		}
	| COND
	;

COND: EXPR '>' EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, ">");
		}
	| EXPR '<' EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "<");
		}
	| EXPR EQUAL EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "==");
		}
	| EXPR NEQUAL EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "!=");
		}
	| EXPR GTE EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, ">=");
		}
	| EXPR LTE EXPR
		{
			print_comp ($<a_char>1, $<a_char>3, "<=");
		}
	;

EXPR: EXPR '+' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "add");
		}
	| EXPR '-' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "sub");
		}
	| EXPR '*' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "times");
		}
	| EXPR '/' EXPR
		{
			$<a_char>$ = print_expression($<a_char>1, $<a_char>3, "div");
		}
	| EXPR '%' EXPR
		{
			if ($<a_char>1 == 'f' || $<a_char>3 == 'f') {
				cout << "\tadd $sp,$sp,4\n";
				yyerror("Cannot do floating point modulo");
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\trem $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n"; }
		}
	| '(' EXPR ')' {$<a_char>$ = $<a_char>2;}
	| '-' EXPR %prec UNARY
		{
			if ($<a_char>2 == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tneg.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if ($<a_char>2 == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tneg $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
			$<a_char>$ = $<a_char>2;
		}
	| '+' EXPR %prec UNARY
		{
			if ($<a_char>2 == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tabs.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if ($<a_char>2 == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tabs $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
			$<a_char>$ = $<a_char>2;
		}
	| {cout << "\tsub $sp, $sp, 4\n";} NUMVAL {$<a_char>$ = $<a_char>2;}
	;

%%

int yyerror (const char *msg) {
	fprintf(stderr, "line %d: %s at '%s'\n", yylineno, msg, yytext);
}

void print_data () {
	cout << "\t.data\n";
	for (auto it = mydata.begin(); it != mydata.end(); ++it) {
		if ((*it).type == 'f') {
			printf("fl%i:\t.float %f\n", (*it).index, (*it).f_val);
		} else if ((*it).type == 's') {
			cout << "str" << (*it).index << ":\t.asciiz " << (*it).s_val << "\n";
		}
	}
}

void push_to_myvars (char type, char* name) {
	variable d;
	d.name = strdup(name);
	d.type = type;
	d.offset = fp;
	fp += 4;
	cout << "\tsub $sp, $sp, 4\n";
	myvars.push_back(d);
}

variable* search_for_var (char* searchterm) {
	for (vector<variable>::iterator it = myvars.begin(); it != myvars.end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	return NULL;
}

void floatstuff (float myfloat, variable* myvar) {
	cout << "\ts.s $f0, ($sp)\n";
	if (myvar->type == 'i') {
		cout << "\tl.s $f0,($sp)\n";
		cout << "\tcvt.w.s $f0,$f0\n";
		cout << "\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else {
		cout << "\tlw $t0,($sp)\n";
		cout << "\tsw $t0,-" << myvar->offset << "($fp)\n";
	}
}

void intstuff (int myint, variable* myvar) {
	if (myvar->type == 'f') {
		cout << "\tsw $t0,($sp)\n";
		cout << "\tl.s $f0,($sp)\n";
		cout << "\tcvt.s.w $f0,$f0\n";
		cout << "\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else cout
		<< "\tsw $t0,-" << myvar->offset << "($fp)\n";
}

void create_push_data (char vt, void* value) {
	data d;
	d.type = vt;
	if (vt == 'f') {
		d.f_val = *((float*) value);
		d.index = ++float_count;
	} else if (vt == 's') {
		d.s_val = (char*)value;
		d.index = ++string_count;
	}
	mydata.push_back(d);
}

char print_expression (char var1_type, char var2_type, const char* operation) {
	if (var1_type == 'f' || var2_type == 'f') {
		cout <<"\tl.s $f0,($sp)\n";
		cout <<"\tl.s $f2,4($sp)\n";
		if (var1_type == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
		if (var2_type == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
		if (strcmp(operation, "add") == 0) {
			cout <<"\tadd.s $f0,$f0,$f2\n";
		} else if (strcmp(operation, "sub") == 0) {
			cout <<"\tsub.s $f0,$f2,$f0\n";
		} else if (strcmp(operation, "times") == 0) {
			cout <<"\tmul.s $f0,$f2,$f0\n";
		} else if (strcmp(operation, "div") == 0) {
			cout <<"\tdiv.s $f0,$f2,$f0\n";
		}
		cout <<"\ts.s $f0,4($sp)\n";
		cout <<"\tadd $sp,$sp,4\n";
		return 'f';
	} else {
		cout << "\tlw $t1,($sp)\n";
		cout << "\tlw $t0,4($sp)\n";
		if (strcmp(operation, "add") == 0) {
			cout << "\tadd $t0,$t0,$t1\n";
		} else if (strcmp(operation, "sub") == 0) {
			cout << "\tsub $t0,$t0,$t1\n";
		} else if (strcmp(operation, "times") == 0) {
			cout << "\tmul $t0,$t0,$t1\n";
		} else if (strcmp(operation, "div") == 0) {
			cout << "\tdiv $t0,$t0,$t1\n";
		}
		cout << "\tsw $t0,4($sp)\n";
		cout << "\tadd $sp,$sp,4\n";
		return 'i';
	}
}

void print_comp (char var1_type, char var2_type, const char* symb) {
	if (var1_type == 'f' || var2_type == 'f') {
		cout <<"\tl.s $f2,($sp)\n";
		cout <<"\tl.s $f0,4($sp)\n";
		if (var1_type == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
		if (var2_type == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
		if (strcmp(symb, ">") == 0) {
			cout <<"\tc.lt.s $f2,$f0\n";
		} else if (strcmp(symb, "<") == 0) {
			cout <<"\tc.lt.s $f0,$f2\n";
		} else if (strcmp(symb, ">=") == 0) {
			cout <<"\tc.le.s $f2,$f0\n";
		} else if (strcmp(symb, "<=") == 0) {
			cout <<"\tc.le.s $f0,$f2\n";
		} else if (strcmp(symb, "==") == 0) {
			cout <<"\tc.eq.s $f0,$f2\n";
		} else if (strcmp(symb, "!=") == 0) {
			cout <<"\tc.eq.s $f0,$f2\n";
		}
		cout << "\tbc1t _cmp" << cmp_count << "\n";
		if ((strcmp(symb, "!=") == 0)) {
			cout << "\tli $t0,1\n";
		} else {
			cout << "\tli $t0,0\n";
		}
		cout << "\tb _aftercmp" << cmp_count << "\n";
		cout << "_cmp" << cmp_count << ":\n";
		if ((strcmp(symb, "!=") == 0)) {
			cout << "\tli $t0,0\n";
		} else {
			cout << "\tli $t0,1\n";
		}
		cout << "_aftercmp" << cmp_count << ":\n";
		cout << "\nsw $t0,4($sp)\n";
		cout << "\nadd $sp,$sp,4\n";
		cmp_count++;
	} else {
		cout << "\tlw $t1,($sp)\n";
		cout << "\tlw $t0,4($sp)\n";
		if (strcmp(symb, ">") == 0) {
			cout << "\tsgt $t0,$t0,$t1\n";
		} else if (strcmp(symb, "<") == 0) {
			cout << "\tslt $t0,$t0,$t1\n";
		} else if (strcmp(symb, ">=") == 0) {
			cout << "\tsge $t0,$t0,$t1\n";
		} else if (strcmp(symb, "<=") == 0) {
			cout << "\tsle $t0,$t0,$t1\n";
		} else if (strcmp(symb, "==") == 0) {
			cout << "\tseq $t0,$t0,$t1\n";
		} else if (strcmp(symb, "!=") == 0) {
			cout << "\tsne $t0,$t0,$t1\n";
		}
		cout << "\tsw $t0,4($sp)\n";
		cout << "\tadd $sp,$sp,4\n";
	}
} 
