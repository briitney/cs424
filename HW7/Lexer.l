%{
#include "Lexer.h"
#include "Parser.tab.h"
%}

int [0-9]+
float [0-9]+(\.[0-9]+)?((e|E)(\-|\+)?[0-9]+)?
word [A-Za-z]([A-Za-z]|[0-9])*
string \"([^\n\"])*\"
beginning_comment \/\/.*
wrap_comment \/\*((\*(\**|([^\*/][^\*]*\*))*)|([^\*]([^\*]*|(\*+[^\*/]))*\**))\*\/
delimiter [\ \t\r\n]?

%option yylineno
%%

	/* keywords */

(?i:commencement) {return COMMENCEMENT;}

(?i:ecrivez) {return ECRIVEZ;}

(?i:entier) {yylval.a_char = 'i'; return ENTIER;}

(?i:reel) {yylval.a_char = 'f'; return REEL;}

(?i:pendant) {return PENDANT;}

(?i:si) {return IF;}

(?i:sinon) {return ELSE;}

(==) {return EQUAL;}

(!=) {return NEQUAL;}

(>=) {return GTE;}

(<=) {return LTE;}

(&&) {return AND;}

(\|\|) {return OR;}

	/* values */

(\r\n|\n\r|\r|\n) {}

{int}	{yylval.an_int = atoi(yytext); return INT;}

{float}	{yylval.a_float = atof(yytext); return FLOAT;}

{word}	{yylval.a_string = strdup(yytext); return WORD;}

{string}	{yylval.a_string = strdup(yytext); return STRING;}

{wrap_comment}|{beginning_comment}	{}

[!(){};=,\+\-\*\/%<>]	{return yytext[0];}

[\ \t]	{}

#.* {cerr << "line " << yylineno << ": parse error at\'" << yytext << "\'" << endl;}

%%
int yywrap () {
	return 1;
}

int main () {
	yyparse();
}
