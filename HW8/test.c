#include "Lexer.h"
forward_list<char*> compiler_vars;

int main (int argc, char* argv[]) {
	cerr_comp_vars();
	compiler_vars.push_front((char*)"VAR");
	cerr_comp_vars();
	compiler_vars.remove((char*)"VAR");
	cerr_comp_vars();
}

void cerr_comp_vars() {
	cerr << "list: ";
	for (auto it = compiler_vars.begin(); it != compiler_vars.end(); ++it) {
		cerr << (*it) << " ";
	}
	cerr << endl;

}
