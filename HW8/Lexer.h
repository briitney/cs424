#ifndef SIMPLELEXER_H
#define SIMPLELEXER_H

#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
using namespace std;

#include <forward_list>

// For writing permissions
#include <fcntl.h>

int countchar (string s, char a);
string stringify (int a);
int in_list(forward_list<char*> list, char* test);
void cerr_comp_vars();
bool comp_equal_to (const char* value);
int newfile(char *fn);
int popfile();

#endif
