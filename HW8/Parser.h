#include <iostream>
#include <string>
#include <string.h>
#include <forward_list>
#include <algorithm>
#include <vector>

#include <stdlib.h>

#include <strings.h>

using namespace std;

struct variable;

void print_data();

void push_to_myvars (char type, char* name);

variable* search_for_var(char* searchterm);

void intstuff (int myint, variable* myvar); 

void floatstuff (float myfloat, variable* myvar);

void create_push_data (char vt, void* value);

char print_expression(char var1_type, char var2_type, const char* operation);

void print_comp (char var1_type, char var2_type, const char* operation);

