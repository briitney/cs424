%{
#include "Parser.h"

using namespace std;

int yyerror (const char * er);
extern int yylex ();
extern int yylineno;
extern char * yytext;
int string_count = 0;
int float_count = 0;
int fp = 4;

struct variable {
    char* name;
    char type;
    int offset;
};

// I know I'm wasting space by having both a float and a char* when I need one or the other
// The type and number are combined to make the label for the data
struct data {
    float f_val;
    char* s_val;
    char type;
    int index;
};

// Anything being stored as a varriable needs to know its name, what type of varriable it is, and its position on the stack.
vector<variable> myvars;

// Anything being put on the data segment just needs to know its type and value
vector<data> mydata;

// Used to keep track of what type the union is using
// Probably a better way to do this, but this works fine for variable asignments
char valuetype;

%}
%union {
  int an_int;
  float a_float;
  char *a_string;
  char type;
}

%left '+' '-'
%left '%' '*' '/'
%nonassoc UNARY

%token <an_int> INT
%token <a_float> FLOAT
%token <a_string> STRING
%token <a_string> WORD
%token UNARY
%token COMMENCEMENT
%token ECRIVEZ
%token <type> ENTIER
%token <type> REEL

%%

PROGRAM : 
	{
		// Start the main method of the program
		cout << "\t.text\n\t.globl main\n";
	}
	FUNCTION_DECLS
	{
		cout << "\tli $v0, 10\n\tsyscall\n";
		print_data();
	}

FUNCTION_DECLS : FUNCTION_DECLS FUNCTION_DECL
	|
	;

FUNCTION_DECL : VALUE WORD '(' ')' '{' STATEMENTS '}'
	| VALUE COMMENCEMENT 
		{
			cout << "main:\n";
			// Initialize the frame pointer to the stack pointer
			cout << "\tmove $fp, $sp\n";
		}
		'(' ')' '{' STATEMENTS '}' 
	;

STATEMENTS : STATEMENTS STATEMENT ';'
	|
	;
	
STATEMENT : FUNCTION_CALL
	| VARIABLE_ASIN
	| VARIABLE_DECL
	| error
	;

TYPE : ENTIER
	| REEL
	;

VARIABLE_DECL : TYPE WORD_LIST

VARIABLE_ASIN : WORD '=' EXPRESSION
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if (valuetype == 'i') {
				cout << "\tlw $t0, ($sp)\n";
				intstuff ($<an_int>3, myvar);
			} else if (valuetype == 'f') {
				create_push_data(valuetype, &($<a_float>3));
				cout << "\tl.s $f0, ($sp)\n";
				floatstuff ($<a_float>3, myvar);
			} else if (valuetype == 'w') {
				variable* twovar = search_for_var($<a_string>3);
				if (twovar == NULL) {
					yyerror("Variable not declaired.");
					return 0;
				}
				if (twovar->type == 'i') {
					cout << "\tlw $t0,-" << twovar->offset << "($fp)\n";
					intstuff (0, myvar);
				}
				if (twovar->type == 'f') {
					cout << "\tl.s $f0,-" << twovar->offset << "($fp)\n";
					floatstuff (0, myvar);
				}
			}
			cout << "\tadd $sp,$sp,4\n";
		}

FUNCTION_CALL : ECRIVEZ '(' PRINT_TYPE ')'
	| WORD '(' VALUES ')'
	;

PRINT_TYPE : STRING  
		{
			create_push_data('s', $1);
			cout << "\tli $v0,4\n";
			cout << "\tla $a0,str" << string_count << "\n";
			cout << "\tsyscall\n";
		}
	| EXPRESSION
		{
			if (valuetype == 'i') {
				cout << "\tli $v0,1\n";
				cout << "\tlw $a0,($sp)\n";
				cout << "\tsyscall\n";
			}
			if (valuetype == 'f') {
				cout << "\tli $v0,2\n";
				cout << "\tl.s $f12,($sp)\n";
				cout << "\tsyscall\n";
			}
		}
	;

WORD_LIST : WORD_LIST ',' WORD {
		push_to_myvars($<type>0, $3);
	}
	| WORD {
		push_to_myvars($<type>0, $1);
	}
	;

VALUES : VALUES ',' VALUE
	| VALUE
	;

VALUE : INT {valuetype = 'i';}
	| FLOAT {valuetype = 'f';}
	| STRING {valuetype = 's';}
	| WORD {valuetype = 'w';}
	;

NUMVAL : INT
		{
			valuetype = 'i';
			cout << "\tli $t0," << $1 << "\n";
			cout << "\tsw $t0,($sp)\n";
		}
	| FLOAT 
		{
			valuetype = 'f';
			create_push_data('f', &($1));
			cout << "\tl.s $f0,fl"<< float_count <<"\n";
			cout << "\ts.s $f0,($sp)\n";
		}
	| WORD 
		{
			variable* myvar = search_for_var($1);
			if (myvar == NULL) {
				yyerror("Variable not declaired.");
				return 0;
			}
			if (myvar->type == 'i') {
				valuetype = 'i';
				cout << "\tlw $t0,-" << myvar->offset << "($fp)\n";
				cout << "\tsw $t0,($sp)\n";
			}
			if (myvar->type == 'f') {
				valuetype = 'f';
				cout << "\tl.s $f0,-" << myvar->offset << "($fp)\n";
				cout << "\ts.s $f0,($sp)\n";
			}
		}
	;

EXPRESSION: EXPRESSION '+' EXPRESSION
		{
			if ($<type>1 == 'f' || $<type>3 == 'f') {
				cout <<"\tl.s $f0,($sp)\n";
				cout <<"\tl.s $f2,4($sp)\n";
				if ($<type>1 == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
				if ($<type>3 == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
				cout <<"\tadd.s $f0,$f0,$f2\n";
				cout <<"\ts.s $f0,4($sp)\n";
				cout <<"\tadd $sp,$sp,4\n";
				valuetype = 'f';
			} else {
				cout << "\tlw $t1,($sp)\n" << endl;
				cout << "\tlw $t0,4($sp)\n" << endl;
				cout << "\tadd $t0,$t0,$t1\n" << endl;
				cout << "\tsw $t0,4($sp)\n" << endl;
				cout << "\tadd $sp,$sp,4\n" << endl;
			}
		}
	| EXPRESSION '-' EXPRESSION
		{
			if ($<type>1 == 'f' || $<type>3 == 'f') {
				cout <<"\tl.s $f0,($sp)\n";
				cout <<"\tl.s $f2,4($sp)\n";
				if ($<type>1 == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
				if ($<type>3 == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
				cout <<"\tsub.s $f0,$f2,$f0\n";
				cout <<"\ts.s $f0,4($sp)\n";
				cout <<"\tadd $sp,$sp,4\n";
				valuetype = 'f';
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\tsub $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n";
			}
		}
	| EXPRESSION '*' EXPRESSION
		{
			if ($<type>1 == 'f' || $<type>3 == 'f') {
				cout <<"\tl.s $f0,($sp)\n";
				cout <<"\tl.s $f2,4($sp)\n";
				if ($<type>1 == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
				if ($<type>3 == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
				cout <<"\tmul.s $f0,$f2,$f0\n";
				cout <<"\ts.s $f0,4($sp)\n";
				cout <<"\tadd $sp,$sp,4\n";
				valuetype = 'f';
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\tmul $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n"; }
		}
	| EXPRESSION '/' EXPRESSION
		{
			if ($<type>1 == 'f' || $<type>3 == 'f') {
				cout <<"\tl.s $f0,($sp)\n";
				cout <<"\tl.s $f2,4($sp)\n";
				if ($<type>1 == 'i') cout <<"\tcvt.s.w $f2,$f2\n";
				if ($<type>3 == 'i') cout <<"\tcvt.s.w $f0,$f0\n";
				cout <<"\tdiv.s $f0,$f2,$f0\n";
				cout <<"\ts.s $f0,4($sp)\n";
				cout <<"\tadd $sp,$sp,4\n";
				valuetype = 'f';
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\tdiv $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n"; }
		}
	| EXPRESSION '%' EXPRESSION
		{
			if ($<type>1 == 'f' || $<type>3 == 'f') {
				cout << "\tadd $sp,$sp,4\n";
				yyerror("Cannot do floating point modulo");
			} else {
				cout << "\tlw $t1,($sp)\n";
				cout << "\tlw $t0,4($sp)\n";
				cout << "\trem $t0,$t0,$t1\n";
				cout << "\tsw $t0,4($sp)\n";
				cout << "\tadd $sp,$sp,4\n"; }
		}

	| '(' EXPRESSION ')'
	| '-' EXPRESSION %prec UNARY
		{
			if (valuetype == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tneg.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if (valuetype == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tneg $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
		}
	| '+' EXPRESSION %prec UNARY
		{
			if (valuetype == 'f') {
				cout << "\tl.s $f0,($sp)\n";
				cout << "\tabs.s $f0, $f0\n";
				cout << "\ts.s $f0, ($sp)\n";
			}
			if (valuetype == 'i') {
				cout << "\tlw $t0,($sp)\n";
				cout << "\tabs $t0,$t0\n";
				cout << "\tsw $t0,($sp)\n";
			}
		}
	| {cout << "\tsub $sp, $sp, 4\n";} NUMVAL {$<type>$ = valuetype;}
	;

%%

int yyerror (const char *msg) {
	fprintf(stderr, "line %d: %s at '%s'\n", yylineno, msg, yytext);
}

void print_data () {
	cout << "\t.data\n";
	for (auto it = mydata.begin(); it != mydata.end(); ++it) {
		if ((*it).type == 'f') {
			printf("fl%i:\t.float %f\n", (*it).index, (*it).f_val);
		} else if ((*it).type == 's') {
			cout << "str" << (*it).index << ":\t.asciiz " << (*it).s_val << "\n";
		}
	}
}

void push_to_myvars (char type, char* name) {
	variable d;
	d.name = strdup(name);
	d.type = type;
	d.offset = fp;
	fp += 4;
	cout << "\tsub $sp, $sp, 4\n";
	myvars.push_back(d);
}

variable* search_for_var (char* searchterm) {
	for (vector<variable>::iterator it = myvars.begin(); it != myvars.end(); ++it) {
		if (strcasecmp((*it).name, searchterm) == 0) return &(*it);
	}
	return NULL;
}

void floatstuff (float myfloat, variable* myvar) {
	if (myfloat < 0) {
		cout << "\tneg.s $f0, $f0\n";
		myfloat = -myfloat;
	}
	cout << "\ts.s $f0, ($sp)\n";
	if (myvar->type == 'i') {
		cout << "\tl.s $f0,($sp)\n\tcvt.w.s $f0,$f0\n\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else {
		cout << "\tlw $t0,($sp)\n\tsw $t0,-" << myvar->offset << "($fp)\n";
	}
}

void intstuff (int myint, variable* myvar) {
	if (myint < 0) {
		cout << "\tneg $t0, $t0\n";
	}
	if (myvar->type == 'f') {
		cout << "\tsw $t0,($sp)\n\tl.s $f0,($sp)\n\tcvt.s.w $f0,$f0\n\ts.s $f0,-" << myvar->offset << "($fp)\n";
	} else cout << "\tsw $t0,-" << myvar->offset << "($fp)\n";

}

void create_push_data (char vt, void* value) {
	data d;
	d.type = vt;
	if (vt == 'f') {
		d.f_val = *((float*) value);
		d.index = ++float_count;
	} else if (vt == 's') {
		d.s_val = (char*)value;
		d.index = ++string_count;
	}
	mydata.push_back(d);
}
