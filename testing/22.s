	.text
	.globl main
main:
	move $fp, $sp
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
_begwhile0:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,100
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sle $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile0
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
_begwhile1:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,7
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile1
	li $v0,4
	la $a0,str1
	syscall
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,7
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	b _begwhile1
_endwhile1:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
_begwhile2:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sgt $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile2
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,10
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,7
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif0
	li $v0,4
	la $a0,str2
	syscall
_falseif0:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,10
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	b _begwhile2
_endwhile2:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
_begwhile3:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,5
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile3
	li $v0,4
	la $a0,str3
	syscall
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,5
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	b _begwhile3
_endwhile3:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
_begwhile4:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sgt $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile4
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,10
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,5
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif1
	li $v0,4
	la $a0,str4
	syscall
_falseif1:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,10
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	div $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	b _begwhile4
_endwhile4:
	li $v0,4
	la $a0,str5
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	b _begwhile0
_endwhile0:
	li $v0, 10
	syscall
	.data
str1:	.asciiz "-BUZZ"
str2:	.asciiz "-BUZZ"
str3:	.asciiz "-FIZZ"
str4:	.asciiz "-FIZZ"
str5:	.asciiz "\n"
