	.text
	.globl main
main:
	move $fp, $sp
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
_begwhile0:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,100
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sle $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile0
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-12($fp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,2
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
_begwhile1:
	sub $sp, $sp, 4
	lw $t0,-12($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
lw $t0,($sp)
beq $t0,0,_skipand0
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	slt $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
_skipand0:
	beq $t0,0,_endwhile1
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	rem $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif0
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-12($fp)
	add $sp,$sp,4
_falseif0:
	sub $sp, $sp, 4
	lw $t0,-8($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-8($fp)
	add $sp,$sp,4
	b _begwhile1
_endwhile1:
	sub $sp, $sp, 4
	lw $t0,-12($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	seq $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif1
	li $v0,4
	la $a0,str1
	syscall
_falseif1:
	beq $t0,1,_falseelse1
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sgt $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif2
	li $v0,4
	la $a0,str2
	syscall
_falseif2:
_falseelse1:
	li $v0,4
	la $a0,str3
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	b _begwhile0
_endwhile0:
	li $v0, 10
	syscall
	.data
str1:	.asciiz "-COMPOSITE"
str2:	.asciiz "-PRIME"
str3:	.asciiz "\n"
