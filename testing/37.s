	sub $sp, $sp, 4
_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,0
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
_begwhile0:
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,10
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sle $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_endwhile0
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str1
	syscall
	sub $sp, $sp, 4
	sub $sp,$sp,8
	lw $t0,-4($fp)
	sw $t0,($sp)
	jal _fnFIB

	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str2
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	sub $sp, $sp, 4
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	add $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	b _begwhile0
_endwhile0:
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
	sub $sp, $sp, 4
_fnFIB:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sub $sp, $sp, 4
	sw $t0,($sp)
	li $t0,1
	sw $t0,($sp)
	lw $t1,($sp)
	lw $t0,4($sp)
	sle $t0,$t0,$t1
	sw $t0,4($sp)
	add $sp,$sp,4
	beq $t0,0,_falseif0
	sub $sp,$sp,4
	lw $t0,-4($fp)
	sw $t0,($sp)
	l.s $f0,($sp)
	s.s $f0,8($fp)
	b _retFIB
_falseif0:
	sub $sp,$sp,4
	sub $sp,$sp,8
	lw $t0,-4($fp)
	sw $t0,($sp)
_retFIB:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
str1:	.asciiz "\t"
str2:	.asciiz "\n"
