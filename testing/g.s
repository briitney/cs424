	sub $sp, $sp, 4
_fnCOMMENCEMENT:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	sub $sp,$sp,8
	lw $t0,-4($fp)
	sw $t0,($sp)
	jal _fnDICKS

	add $sp,$sp,4
	li $v0,4
	la $a0,str1
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str2
	syscall
_retCOMMENCEMENT:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
	sub $sp, $sp, 4
_fnDICKS:
	sw $fp,8($sp)
	sw $ra,4($sp)
	la $fp,4($sp)
	sub $sp, $sp, 4
	sub $sp, $sp, 4
	li $t0,4
	sw $t0,($sp)
	lw $t0, ($sp)
	sw $t0,-4($fp)
	add $sp,$sp,4
	li $v0,4
	la $a0,str3
	syscall
	sub $sp, $sp, 4
	lw $t0,-4($fp)
	sw $t0,($sp)
	li $v0,1
	lw $a0,($sp)
	syscall
	li $v0,4
	la $a0,str4
	syscall
	sub $sp,$sp,4
	lw $t0,-4($fp)
	sw $t0,($sp)
	l.s $f0,($sp)
	s.s $f0,8($fp)
	b _retDICKS
_retDICKS:
	move $sp,$fp
	lw $fp,4($sp)
	lw $ra,($sp)
	add $sp,$sp,8
	jalr $ra
main:
	move $fp, $sp
	sub $sp,$sp,8
	jal _fnCOMMENCEMENT
	li $v0,10
	syscall
	.data
str1:	.asciiz "a: "
str2:	.asciiz "\n"
str3:	.asciiz "a: "
str4:	.asciiz "\n"
