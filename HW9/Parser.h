#include <iostream>
#include <stdio.h>
#include <string>
#include <string.h>
#include <forward_list>
#include <algorithm>
#include <vector>

#include <stdlib.h>

#include <strings.h>

using namespace std;

struct variable;

struct function;

void print_data();

void push_to_vars (char type, char* name, void* list, int incr);

variable* search_for_var (char* searchterm);

void intstuff (int myint, variable* myvar); 

void floatstuff (float myfloat, variable* myvar);

void create_push_data (char vt, void* value);

char print_expression(char var1_type, char var2_type, const char* operation);

void print_comp (char var1_type, char var2_type, const char* operation);

int check_myfuncs(char ret_type, char* name, void* parameters);

void add_myfuncs(char ret_type, char* name, void* parameters, int defnd);

char* str_upr(char* str);

function* get_myfuncs(char* name);

void print_myfuncs();

void print_cur_param();

int check_type_arity(char* func_name);
