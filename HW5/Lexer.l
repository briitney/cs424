%{
#include "Lexer.h"
#include "types.h"
#include "Parser.tab.h"
%}

int (\-|\+)?[0-9]+
float (\-|\+)?[0-9]+(\.[0-9]+)?((e|E)(\-|\+)?[0-9]+)?
word [A-Za-z]([A-Za-z]|[0-9])*
string \"([^\n\"])*\"
beginning_comment \/\/.*
wrap_comment \/\*((\*(\**|([^\*/][^\*]*\*))*)|([^\*]([^\*]*|(\*+[^\*/]))*\**))\*\/
delimiter [\ \t\r\n]?

%option yylineno
%%

	/* keywords */

(?i:commencement)/{delimiter} {return COMMENCEMENT;}

(?i:ecrivez)/{delimiter} {return ECRIVEZ;}

(?i:entier)/{delimiter} {yylval.type = 'i'; return ENTIER;}

(?i:reel)/{delimiter} {yylval.type = 'f'; return REEL;}
	/* values */

(\r\n|\n\r|\r|\n) {}

{int}/{delimiter}	{yylval.an_int = atoi(yytext); return INT;}

{float}/{delimiter}	{yylval.a_float = atof(yytext); return FLOAT;}

{word}/{delimiter}	{yylval.a_string = strdup(yytext); return WORD;}

{string}/{delimiter}	{yylval.a_string = strdup(yytext); return STRING;}

{wrap_comment}|{beginning_comment}/{delimiter}	{}

[(){};=,]	{return yytext[0];}

[\ \t]	{}

[^\ \t\r\n(){};=,]+/{delimiter} {cerr << "line " << yylineno << ": parse error at\'" << yytext << "\'" << endl;}

%%
int yywrap () {
	return 1;
}

int main () {
	yyparse();
}
